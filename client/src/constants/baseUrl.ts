export const baseUrl = process.env.NODE_ENV === 'production' ? 'https://it-stone.herokuapp.com/' : 'http://localhost:3030';
