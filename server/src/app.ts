import 'reflect-metadata';

import { InversifyExpressServer } from 'inversify-express-utils';

import bodyParser from 'body-parser';
import morgan from 'morgan';
import cors from 'cors';
import express from 'express';
import path from 'path';

import './controller';
import { CONTAINER } from './service/services-registration';

const server = new InversifyExpressServer(CONTAINER);

server.setConfig((app) => {
    app.use(morgan('dev'));
    app.use(cors());
    app.use(bodyParser.urlencoded({
        extended: true
    }));
    app.use(bodyParser.json());

    if (process.env.NODE_ENV === 'production') {
        app.use(express.static(path.join(__dirname, '../../client/build')));
    }
});

const application = server.build();

const port = process.env.PORT || 3030;
const serverInstance = application.listen(port, () => {
    console.log('Press CTRL+C to stop\n');
});
